---
layout: markdown_page
title: "Sales Process"
---
## Helpful links for prospects and customers

1. [GitLab subscription page](https://about.gitlab.com/subscription/)

1. [GitLab HA page](https://about.gitlab.com/high-availability/)

1. [EE repo members](https://gitlab.com/subscribers/gitlab-ee/team)

1. [Standard subscribers list](https://gitlab.com/groups/standard/members)

1. [GitLab CE issues list](https://gitlab.com/gitlab-org/gitlab-ce/issues)

1. [GitLab.com support forum](https://gitlab.com/gitlab-com/support-forum/issues)

1. [GitLab feedback tracker](http://feedback.gitlab.com/forums/176466-general)

1. [GitLab documentation](http://doc.gitlab.com/)

1. [Offer for university students](https://about.gitlab.com/2014/05/19/students-now-free/)

1. [YouTube page](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg)

1. [GitLab architecture for noobs (office analogy)](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/doc/development/architecture.md)

1. [GitLab flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)

1. [Free GitLab workshop on Platzi (Job as trainer)](https://courses.platzi.com/courses/git-gitlab/)

## Migrating from Highrise to Streak

1. All new emails go to Streak

1. Highrise pending deal: make box when a pending deal task is addressed

1. Highrise completed deal: make box when renewal is due (3 months in)

## Streak workflow

1. All leads get boxes

1. Leads transform to opportunities and are included in the forecast when they become Sales Qualified Leads

1. An organization is created in the organization pipeline when one company gets more than one opportunity

1. Opportunities are created for renewal and updates

1. Follow-up tasks are made just for opportunities

## Generating leads

[Types of leads](/handbook/sales_process/generating_leads).

## Creating deals

Every time a deal is created, a task is also created. The task succinctly describes [next action](/handbook/sales_process/creating_deals).

## Deal management process

Find out [how to manage deals](/handbook/sales_process/deal_management_process) with potential customers.

## Account management

Aspects to consider when [managing an account](/handbook/sales_process/account_management).

## Emailing

[Customer emailing guidelines](/handbook/sales_process/emailing).

## Accounting

[Accounting procedures](/handbook/sales_process/accounting).

## Licenses

Everything about [lincenses and EE access](/handbook/sales_process/licenses).

## Deprecated

### Invoice in Twinfield for clients paying by bank (non-Recurly)

* This is part of the [won subscription deal procedure](/handbook/sales_process/deal_management_process#won-subscription-deal-procedure-invoicing), you should have done a VIES check already if EU client

* [Login Twinfield](https://login.twinfield.com/) (SUPER, het wachtwoord, COMCOASTER, Nederlands)

* "Browser not supported" click on “Ga door naar Twinfield”

* Select GitLab B.V.

* Make sure you have an invoice address and for EU countries a VAT number

* If payment will be made in USD, then google for "1 EUR in USD"

* General => Company => Currencies => USD => Exchange rates => +Line

* Credit management > Sales Invoice Dashboard > Sales > New invoice

* First, select a customer. If customer does not exist, press + New customer

* Name: Company name

* Billing email: leave blank

* Fill in address, no need to fill in bank information

* Make sure to insert VAT number if EU customer

* Press “create”

* Reference: PO number if PO exists

* Invoice date: today (always effective date of today)

* Due date: 30 days from invoice date

* Quantity: Number of packs for subscription, number of hours for consultancy

* Description: Options self explanatory. If item does not exist, create it together with Sytse

* Price: change to $, add correct price

* VAT:

![VAT](/handbook/sales_process/images_sales_process/vat2_sales_process.png) 

* If ICL was chosen, press services

* Add closing text:

1. For non-EU clients: No VAT according to article 44 and 59 of the European VAT Directive.

1. For EU clients: VAT shifted to recipient according to article 44 and 196 of the European VAT Directive.

1. Dutch clients: leave blank

* Press “create” to create a provisional invoice;

* Press “send by email as PDF” to finalize, in popup email write own email address.

### Crediting an invoice made in Twinfield (DEPRECATED)

1. If you are correcting a mistake,keep the credit invoice in draft for review.

1. Assuming the new invoicing dashboard

1. Open the old invoice

1. Press the credit button

1. Set invoice date to today if the customer cancelled, if you are undoing a mistake put in the same date as the incorrect invoice

1. Fill out the ICL details if needed (Services, date of new invoice)

1. Send PDF

1. Send the credit invoice to the customer (this is legally required)
